// $Id: listmap.tcc,v 1.7 2015-04-28 19:22:02-07 - - $

/**
 * Edited by Arnaldo Carneiro (ardasilv)
 * 1506372
 */

#include "listmap.h"
#include "trace.h"

//
/////////////////////////////////////////////////////////////////
// Operations on listmap::node.
/////////////////////////////////////////////////////////////////
//

//
// listmap::node::node (link*, link*, const value_type&)
//
template <typename Key, typename Value, class Less>
listmap<Key,Value,Less>::node::node (node* next, node* prev,
                                     const value_type& value):
            link (next, prev), value (value) {
}

//
/////////////////////////////////////////////////////////////////
// Operations on listmap.
/////////////////////////////////////////////////////////////////
//

//
// listmap::~listmap()
//
template <typename Key, typename Value, class Less>
listmap<Key,Value,Less>::~listmap() {
   TRACE ('l', (void*) this);
   TRACE ('v', "*** DELETING " << this << " ***");
   while (begin() != end()) {
      erase(begin());
   }
}

//
// iterator listmap::insert (const value_type&)
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::iterator
listmap<Key,Value,Less>::insert (const value_type& pair) {
   TRACE ('l', &pair << "->" << pair);
   TRACE ('v', "*** INSERTING " << pair << " ***");
   cout << pair << endl;
   node* no = new node(nullptr, nullptr, pair);
   if (begin() == end()) {
      no->next = anchor();
      no->prev = anchor();
      anchor()->next = no;
      anchor()->prev = no;
   }
   else {
      node* curr = anchor()->next;
      while (curr != anchor() and
             less (curr->value.first, pair.first)) {
         curr = curr->next;
      }
      if (curr == anchor() or
          less (pair.first, curr->value.first)) {
         no->next = curr;
         no->prev = curr->prev;
         no->next->prev = no;
         no->prev->next = no;
      }
      else if (pair.first == curr->value.first) {
         curr->value.second = pair.second;
         return (curr);
      }
   }
   return iterator(no);
}

//
// listmap::find(const key_type&)
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::iterator
listmap<Key,Value,Less>::find (const key_type& that) {
   TRACE ('l', that);
   TRACE ('v', "*** FINDING " << that << " ***");
   node* curr = anchor()->next;
   while (curr != anchor() and less (curr->value.first, that)) {
      curr = curr->next;
   }
   if (curr != anchor() and curr->value.first == that) {
      return iterator(curr);
   }
   else
      return iterator(end());
}

//
// iterator listmap::erase (iterator position)
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::iterator
listmap<Key,Value,Less>::erase (iterator position) {
   TRACE ('l', &*position);
   TRACE ('v', "*** DELETING " << &*position << " ***");
   node* no = (node*) (&*position - 1);
   node* tmp = no->next;
   no->prev->next = no->next;
   no->next->prev = no->prev;
   delete no;
   return iterator(tmp);
}

//
/////////////////////////////////////////////////////////////////
// Operations on listmap::iterator.
/////////////////////////////////////////////////////////////////
//

//
// listmap::value_type& listmap::iterator::operator*()
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::value_type&
listmap<Key,Value,Less>::iterator::operator*() {
   TRACE ('l', where);
   return where->value;
}

//
// listmap::value_type* listmap::iterator::operator->()
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::value_type*
listmap<Key,Value,Less>::iterator::operator->() {
   TRACE ('l', where);
   return &(where->value);
}

//
// listmap::iterator& listmap::iterator::operator++()
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::iterator&
listmap<Key,Value,Less>::iterator::operator++() {
   TRACE ('l', where);
   where = where->next;
   return *this;
}

//
// listmap::iterator& listmap::iterator::operator--()
//
template <typename Key, typename Value, class Less>
typename listmap<Key,Value,Less>::iterator&
listmap<Key,Value,Less>::iterator::operator--() {
   TRACE ('l', where);
   where = where->prev;
   return *this;
}


//
// bool listmap::iterator::operator== (const iterator&)
//
template <typename Key, typename Value, class Less>
inline bool listmap<Key,Value,Less>::iterator::operator==
            (const iterator& that) const {
   return this->where == that.where;
}

//
// bool listmap::iterator::operator!= (const iterator&)
//
template <typename Key, typename Value, class Less>
inline bool listmap<Key,Value,Less>::iterator::operator!=
            (const iterator& that) const {
   return this->where != that.where;
}
