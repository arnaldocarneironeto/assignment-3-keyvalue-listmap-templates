// $Id: main.cpp,v 1.8 2015-04-28 19:23:13-07 - - $

/**
 * Edited by Arnaldo Carneiro (ardasilv)
 * 1506372
 */

#include <cstdlib>
#include <exception>
#include <iostream>
#include <string>
#include <unistd.h>
#include <fstream>
#include <algorithm>

using namespace std;

#include "listmap.h"
#include "xpair.h"
#include "util.h"

using str_str_map = listmap<string,string>;
using str_str_pair = str_str_map::value_type;

void scan_options (int argc, char** argv) {
   opterr = 0;
   for (;;) {
      int option = getopt (argc, argv, "@:");
      if (option == EOF) break;
      switch (option) {
         case '@':
            traceflags::setflags (optarg);
            break;
         default:
            complain() << "-" << (char) optopt << ": invalid option"
                       << endl;
            break;
      }
   }
}

inline std::string trim(const std::string &s) {
   std::string whitespaces (" \t\f\v\n\r");
   auto sfront = s.find_first_not_of(whitespaces);
   auto sback = s.find_last_not_of(whitespaces) + 1;
   return (sback <= sfront ?
           ""   :
           s.substr(sfront, sback - sfront));
}

const string cin_name      = "-";
const string hash_symbol   = "#";
const string equals_symbol = "=";
const string separator     = ": ";
         
void process_file(istream& inputfile,
                  const string& filename) {
   str_str_map test;
   size_t inputlinenumber {1};
   while(!inputfile.eof()) {
      string inputline;
      getline(inputfile, inputline);
      cout << filename << separator << inputlinenumber++ <<
              separator << inputline << endl;
      inputline = trim(inputline);
      if (!inputline.empty() and inputline.find(hash_symbol) != 0) {
         size_t position = inputline.find_first_of(equals_symbol);
         if (position != string::npos) {
            string key {trim(inputline.substr(0, position))};
            string value {trim(inputline.substr(position + 1,
                               inputline.size()))};
            if (key.empty())
               if (value.empty())
                  for(const auto& pair: test)
                     cout << pair << endl;
               else {
                  for(const auto& pair: test)
                     if (pair.second == value)
                        cout << pair << endl;
               }
            else
               if (value.empty())
               {
                  auto no = test.find (key);
                  if (no != test.end())
                     test.erase(no);
               }
               else {
                  str_str_pair pair (key, value);
                  test.insert(pair);
               }
         }
         else {
            auto no = test.find(inputline);
            if (no != test.end())
               cout << *no << endl;
            else
               cout << inputline << ": key not found" << endl;
         }
      }
   }
}

string get_executable_name(char slash, string executable_name) {
   string result {executable_name};
   size_t position = result.find(slash);
   while (position != string::npos)
   {
      result = result.substr(position + 1, result.size());
      position = result.find(slash);
   }
   return {result};
}

int main (int argc, char** argv) {
   sys_info::set_execname (argv[0]);
   scan_options (argc, argv);

   for (char** argp = &argv[optind]; argp != &argv[argc]; ++argp) {
      ifstream inputfile (*argp);
      if (inputfile.is_open()) {
         process_file(inputfile, *argp);
         inputfile.close();
      }
      else {
         string executable_name {get_executable_name('\\',
                                 get_executable_name('/', argv[0]))};
         cerr << executable_name << separator << *argp << separator
              << "No such file or directory" << endl;
         return EXIT_FAILURE;
      }
   }
   if (optind == argc) { // no input file given on command line
      process_file(cin, cin_name);
   }

   /*for (str_str_map::iterator itor = test.begin();
        itor != test.end(); ++itor) {
      cout << "During iteration: " << *itor << endl;
   }

   str_str_map::iterator itor = test.begin();
   test.erase (itor);*/

   return EXIT_SUCCESS;
}
